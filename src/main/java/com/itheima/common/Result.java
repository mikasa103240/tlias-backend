package com.itheima.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer code;
    private String msg;
    private T data;

    /**
     * 增删改 成功响应
     *
     * @return
     */
    public static Result<?> success() {
        return new Result<>(1, "success", null);
    }

    /**
     * 查询 成功响应
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(1, "success", data);
    }

    /**
     * 失败响应
     *
     * @param msg
     * @return
     */
    public static Result<?> error(String msg) {
        return new Result<>(0, msg, null);
    }
}
